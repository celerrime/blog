+++
title = "Using Netlify"
date = 2020-01-28T19:32:35Z
draft = false
categories = ["About", "Software Tools"]
tags = ["netlify"]
summary = "Why Netlify?"
subtitle = "test subtitle"
+++

# What is Netlify

> All-in-one platform for automating modern web projects

Netlify is a platform that provides $0/month hosting + additional tools to automate web deployment. Take a look at their [pricing](https://www.netlify.com/pricing) and you'll see that their *Starter* tier provides useful tools at no cost to me.

> #### Starter $0/month
> ###### Great for hosting personal projects, hobby sites, or experiments
> - Custom domains & HTTPS
> - Instant Git integration
> - Continuous deployment
> - Deploy previews
> - Access to add-ons

# Why Netlify

Before Netlify, I used GitHub Pages (also free hosting), but it was bare bones and lacked tools to scale my site. Before GitHub pages, I used [DigitalOcean](https://www.digitalocean.com) as a Virtual Private Server to host and deploy my site for $5/month. 

Netlify provides me not only free hosting, but useful tools and content to help me grow my site. Git integration and continuous deployment were my primary motivations and will keep me here for the time being.


