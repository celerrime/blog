+++
title = "Colophon"
subtitle = ""

# Add a summary to display on homepage (optional).
summary = "How this site is built."

date = 2020-01-28T12:41:10-00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["admin"]

# Is this a featured post? (false/false)
featured = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["hugo", "vscode", "netlify", "notepadium", "gitlab"]
categories = ["Opensource", "About"]
+++

This site utilizes a host of opensource tools to deliver fast & efficient content without any ads. Apart from my time, it costs $15/year to maintain ownership of the domain.

- [Hugo](https://gohugo.io) for statically generating HTML files (opensource & free)
- [Visual Studio Code](https://code.visualstudio.com) or vim as my primary editor (opensource & free)
- [Netlify](https://www.netlify.com/pricing/) as my deployment target (free hosting) - [this post](../using-netlify/) for details 
- [Notepadium](https://themes.gohugo.io/hugo-notepadium/) as the theme (free & opensource)
- [Google Domains](https://domains.google/) as the domain name registrar I purchased [mcastorillo](https://mcastorillo.com/) from
- [Gitlab](https://gitlab.com/celerrime/) as the public repository for this opensource site

Static generators dramatically increase the performance of your website, minimizes the need for running risky JavaScript, and provides simplicity with very little dependencies.

If you'd like your own site, explore the resources above starting with Hugo or contact me.
