+++
title = "Twitter Keywords to Mute"
subtitle = ""

# Add a summary to display on homepage (optional).
summary = "Twitter hacks before they fix it"

date = 2020-01-24T13:14:04-04:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = []

# Is this a featured post? (false/false)
featured = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["twitter"]
categories = ["Hacks"]

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
# projects = ["internal-project"]

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
+++
Mute these keywords to declutter your Twitter feed. They'll eventually fix this bug, but for now, we can get clean feeds.

https://twitter.com/settings/muted_keywords

Unfortunately, you'll have to add them one at a time.
## Keywords to Mute

ActivityTweet
generic_activity_highlights
generic_activity_momentsbreaking
RankedOrganicTweet
suggest_activity
suggest_activity_feed
suggest_activity_highlights
suggest_activity_tweet
suggest_grouped_tweet_hashtag
suggest_pyle_tweet
suggest_ranked_organic_tweet
suggest_ranked_timeline_tweet
suggest_recap
suggest_recycled_tweet
suggest_recycled_tweet_inline
suggest_sc_tweet
suggest_timeline_tweet
suggest_who_to_follow
suggestactivitytweet
suggestpyletweet
suggestrecycledtweet_inline

source: https://gist.github.com/IanColdwater/88b3341a7c4c0cf71c73ac56f9bd36ec

