+++
title = ""
subtitle = ""
summary = ""
date = 2020-01-29T12:41:10-00:00
draft = true
authors = ["admin"]
tags = [""]
categories = [""]
+++

# Positional Paramters

In Bash, positional parameters are

> $0 # the program name
> $1 # the first argument right after the program name
> $2 # second ..
> .
> .


# Template for Command Line Arguments

> #!/bin/bash
>
> if [ "$1" != "" ]; then
>     echo "First parameter is not empty"
> else
>     echo "First parameter is not empty"
> fi

Other variables include:
- $#	# the number of arguments
-   
###### Sources
- http://linuxcommand.org/lc3_wss0120.php
