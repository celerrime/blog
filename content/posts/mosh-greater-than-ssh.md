+++
title = "MOSH greater than SSH"
subtitle = "When MOSH outpaces SSH"
summary = "Use cases for when Mosh is more useful than SSH"
date = 2020-01-29T12:41:10-00:00
draft = false
authors = ["admin"]
tags = ["cli","mosh","ssh"]
categories = ["Command-Line"]
+++

# What is (`mosh`)?

"Mobile Shell (`mosh`) is a robust and responsive replacement for SSH. `mosh` persists connections to remote servers while roaming between networks."

# Why MOSH over SSH?

Unlike ssh, mosh is able to persist connections to remote hosts while localhost switches networks or localhost's network encounters some packet loss.

# Use Cases

This makes mosh extremely useful when connecting to remote hosts over spotty wifi (i.e., on unreliable networks such as in coffee shops, airports, etc.), on cellular networks (i.e., administrating servers on LTE, or in situations where localhost may need to switch networks (i.e., jumping subnets).


