+++
draft = false
+++

Welcome to my personal website. Here, I post about software tools, worksflows, and tutorials on a wide variety of topics - from information security and data science, to photography, and other interests.

This site is more for me, but I hope others will find it useful.

